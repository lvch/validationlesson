<?php

require __DIR__ . '/vendor/autoload.php';

use App\Author;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

author(
    Validation::createValidatorBuilder()
        ->enableAnnotationMapping()
        ->getValidator()
);

function author(ValidatorInterface $validator)
{
    $author = new Author();

    // ... do something to the $author object

    $errors = $validator->validate($author);

    if (count($errors) > 0) {
        /*
         * Uses a __toString method on the $errors variable which is a
         * ConstraintViolationList object. This gives us a nice string
         * for debugging.
         */
        $errorsString = (string) $errors;
        echo $errorsString;

        return;
    }

    echo "Data is valid!!!";
}

$input = [
    'name' => [
        'first_name' => 'Fabien',
        'last_name' => 'Potencier',
    ],
    'email' => 'test@email.tld',
    'simple' => 'hello',
    'eye_color' => 3,
    'file' => null,
    'password' => 'test',
    'tags' => [
        [
            'slug' => 'symfony_doc',
            'label' => 'symfony doc',
        ],
    ],
];

echo "\n\nMy own validator\n\n";
(new \App\Validation\MyValidator())->validate($input);