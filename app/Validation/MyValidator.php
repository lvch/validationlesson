<?php

namespace App\Validation;

use Symfony\Component\Validator\Validation;
use App\Validation\Constraint\MyConstraint;

class MyValidator
{

    public function validate($data)
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($data, MyConstraint::getConstraints());

        if ($violations->count() > 0) {
            echo $violations;
            return;
        }

        echo "Data is valid!!!";
    }
}