<?php

namespace App\Validation\Constraint;

use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\File;

class MyConstraint
{
    public static function getConstraints() :Collection
    {
        return new Collection(
            [
                'fields' => [
                    // the keys correspond to the keys in the input array
                    'name' => new Collection([
                        'first_name' => new Length(['min' => 101]),
                        'last_name' => new Length(['min' => 1]),
                    ]),
                    'email' => new Email(),
                    'simple' => new Length(['min' => 102]),
                    'eye_color' => new Choice([3, 4]),
                    'file' => new File(),
                    'password' => new Length(['min' => 60]),
                    'tags' => new Optional([
                        new Type('array'),
                        new Count(['min' => 1]),
                        new All([
                            new Collection([
                                'slug' => [
                                    new NotBlank(),
                                    new Type(['type' => 'string']),
                                ],
                                'label' => [
                                    new NotBlank(),
                                ],
                            ]),
                        ]),
                    ]),
                ],
                'allowExtraFields' => true,
            ]
        );
    }
}